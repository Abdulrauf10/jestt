const journalController = require ("../controllers/journalController");
const mockRequest = (body = {}) => ({body});
const mockResponse = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
}


test("res.json api", (done) => {
  const req = mockRequest();
  const res = mockResponse();

  journalController.index(req, res);
  expect(res.status).toBeCalledWith(200);

  done()
})