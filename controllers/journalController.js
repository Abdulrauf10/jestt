const {portal} = require ("../models");

module.exports = {
  showJournal: (req, res) => {
    portal.findAll().then((eJournal) => {
      res.json(eJournal)
    }).then(() => {
      res.json({message: "successfuly"})
    }).catch((err) => {
      res.status(500).json({
        result: "FAILED",
        message: err.message || "can not access journal"
      })
    })
  }
}